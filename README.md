# i3wm config
Here is the configuration to i3 window manager. I added programs that are not included in the distribution. It will be required to install majority of the programs to fully utilize my config out of the box. You can use it as a base to customize it yourself to get familiar with i3. I added the programs I personally use on my desktop. 

## Screenshot
![](/screenshot/i3wm.png)

## i3wm config Location:
```
~/.config/i3/config
```

## i3status config Location:
```
~/.config/i3status/config
```

## autostart script Location:
```
~/.config/i3/autostart.sh
```

## Remember to make autostart.sh executable
```
chmod +x ~/.config/i3/autostart.sh
```

## Remember to copy default i3status configuration to ~/.config/i3status
```
cp /etc/i3status.conf ~/.config/i3status/config
```

## Programs:
### Install these programs from your linux distribution package manager.
1. alacritty or terminal emulator of your choice
2. dmenu/rofi
3. neovim/vim or nano
4. nemo
5. chromium/firefox
6. flameshot
7. redshift
8. feh/nitrogen (Recommendation: Use nitrogen if new to the i3 window manager)
9. network-manager-applet
10. udiskie (usb automounting utility)
11. polkit-gnome/policykit-1-gnome
12. pulseaudio
13. unclutter 
14. discord 
15. ulauncher

## Credit to the creators of the i3 window manager
Official Website: https://i3wm.org

Github Repository: https://github.com/i3/i3
