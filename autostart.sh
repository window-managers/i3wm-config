#!/bin/sh
nm-applet &
pasystray &
redshift-gtk &
udiskie &
unclutter &
# Choice between feh or nitrogen
~/.fehbg &
#nitrogen --restore &
ulauncher --hide-window &
#Ubuntu
/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &
# Arch linux
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/geoclue-2.0/demos/agent &
xrdb ~/.Xresources &
